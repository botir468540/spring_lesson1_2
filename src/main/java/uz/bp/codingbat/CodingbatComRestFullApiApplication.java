package uz.bp.codingbat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodingbatComRestFullApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(CodingbatComRestFullApiApplication.class, args);
    }

}
