package uz.bp.codingbat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.bp.codingbat.entity.Subject;

public interface SubjectRepository extends JpaRepository<Subject, Integer> {
    boolean existsByName(String name);
    boolean existsByNameAndIdNot(String name, Integer id);
}
