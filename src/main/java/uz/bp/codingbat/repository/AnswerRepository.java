package uz.bp.codingbat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.bp.codingbat.entity.Answer;

public interface AnswerRepository extends JpaRepository<Answer, Integer> {
    boolean existsByTextAndUserId(String text, Integer user_id);
    boolean existsByTextAndUserIdAndIdNot(String text, Integer user_id, Integer id);

}
