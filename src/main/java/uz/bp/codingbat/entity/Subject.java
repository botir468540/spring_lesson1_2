package uz.bp.codingbat.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import uz.bp.codingbat.entity.template.AbsEntity;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Subject extends AbsEntity {
}
